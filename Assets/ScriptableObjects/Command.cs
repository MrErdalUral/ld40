using UnityEngine;

public abstract class Command : ScriptableObject
{
    public virtual void Init(Command command) { }
    public abstract void Activate(Command command, NpcController npc);
    public abstract void Deactivate(Command command, NpcController npc);
}