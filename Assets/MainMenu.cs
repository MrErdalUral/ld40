﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject InstructionsText;
	public void StartGame()
	{
	    SceneManager.LoadScene("main");
	}
    public void ExitGame()
    {
        Application.Quit();
    }
    public void Instructions()
    {
        InstructionsText.SetActive(!InstructionsText.activeInHierarchy);
    }
}
