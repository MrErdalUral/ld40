using UnityEngine;

public static class MoveCommand
{
    public static void To(NpcController npc, Transform target, bool acceptBuilding = true, bool followCopy = true)
    {
        if (npc.NpcRole != Role.Resistance || !npc)
            return;

        var move = npc.GetComponent<PlayerMove>();
        var follow = npc.GetComponent<NpcFollow>();
        Building building;

        FollowTransform.RemoveFollower(npc);

        if (acceptBuilding && Building.TryGetBuildingAt(target.position, out building))
        {
            building.Blink();
            follow.FollowTarget = followCopy
                ? FollowTransform.AddFollower(building.Door.transform, npc)
                : building.Door.transform;
        }
        else if (target.CompareTag("Player"))
        {
            GameManager.Instance.PlayerTransform.GetComponent<BlinkController>().Blink();
            follow.FollowTarget = target;
        }
        else
        {
            follow.FollowTarget = followCopy
                ? FollowTransform.AddFollower(target, npc)
                : target;
        }

        follow.Follow();
    }

    private static Vector2 MoveToArea(NpcController npc, PlayerMove move, Vector2 target)
    {
        var npcCenter = NpcManager.Instance.ResistanceNpcsCenter;

        var centerNpcOffset = npc.transform.position.xy() - npcCenter;
        var centerNewCenterOffset = target - npcCenter;

        return centerNewCenterOffset + centerNpcOffset;
    }

    private static Vector2 MoveToPoint(NpcController npc, PlayerMove move, Vector2 target)
    {
        return target - npc.transform.position.xy();
    }
}