using UnityEngine;

public class Door : MonoBehaviour
{
    [HideInInspector]
    public Building Building;

    private BoxCollider2D _collider;

    private void Awake()
    {
        Building = GetComponentInParent<Building>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var npc = other.GetComponent<NpcController>();
        if (npc == null)
            return;

        if (npc.NpcRole == Role.Resistance || npc.NpcRole == Role.Police)
        {
            Building.Enter(npc);
        }
    }
}