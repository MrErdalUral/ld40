using System;
using UnityEngine;

public static class Extensions
{
    public static bool Approx(this float f, float other) { return Mathf.Approximately(f, other); }
    public static bool Approx(this float f, float other, float diff) { return Mathf.Abs(f - other) < diff; }
    public static bool Approx0(this float f) { return f.Approx(0); }
    public static bool Approx1(this float f) { return f.Approx(1); }
    
    public static Vector2 xy(this Vector3 v) { return new Vector2(v.x, v.y); }

    public static Vector3 To3d(this Vector2 v, float z = 0)
    {
        return new Vector3(v.x, v.y, z);
    }

    public static bool TagExistsIn(this GameObject go, string[] strings)
    {
        for (var i = strings.Length - 1; i >= 0; i--)
        {
            if (go.CompareTag(strings[i]))
            {
                return true;
            }
        }
        return false;
    }

    public static ulong Delay(this AudioClip clip, float seconds)
    {
        return Convert.ToUInt64(Mathf.Ceil(clip.samples * seconds));
    }

    public static AudioSource RandomizePitch(this AudioSource source, float maxOffset)
    {
        source.pitch = 1 + UnityEngine.Random.Range(0, maxOffset);
        return source;
    }

    public static float Random(this Vector2 v) { return UnityEngine.Random.Range(v.x, v.y); }

    public static T SelectRandom<T>(this T[] array)
    {
        var i = UnityEngine.Random.Range(0, array.Length);
        return array[i];
    }
}
