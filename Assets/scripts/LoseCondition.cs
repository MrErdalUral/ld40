﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NpcHealth))]
public class LoseCondition : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NpcHealth>().Health <= 0)
        {
            GameManager.Instance.Lose();
            Destroy(gameObject);
        }
    }
}
