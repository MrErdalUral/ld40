﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMove : MonoBehaviour
{

    public float Speed;
	private Rigidbody2D _rigidBody;
	
	private void Awake()
	{
		_rigidBody = GetComponent<Rigidbody2D>();
	}
	
    public void Move(Vector2 v)
    {
        _rigidBody.velocity = v.normalized * Speed;
        var angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
		_rigidBody.rotation = angle;
    }
    public void SlowMove(Vector2 v)
    {
        _rigidBody.velocity = v.normalized * Speed / 4;
        var angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
		_rigidBody.rotation = angle;

    }

    public void Stop()
    {
        Move(Vector2.zero);
    }
}
