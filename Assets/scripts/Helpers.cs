public static class Helpers
{
    public static void AssignIfNull<T>(ref T reference) where T : UnityEngine.Object
    {
        if (reference == null)
            reference = UnityEngine.Object.FindObjectOfType<T>();
    }
}