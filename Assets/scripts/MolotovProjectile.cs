﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MolotovProjectile : MonoBehaviour
{

    public GameObject Molotov;
    public float DestroyTime = 2f;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(MolotovRoutine());
        

    }

    private IEnumerator MolotovRoutine()
    {
        yield return new WaitForSeconds(DestroyTime);
        if (Molotov)
            Instantiate(Molotov, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

}
