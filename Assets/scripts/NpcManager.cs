using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class NpcManager : MonoBehaviour
{

    public static NpcManager Instance;

    public GameObject NpcPrefab;

    public List<NpcController> ResistanceNpcList = new List<NpcController>();
    public List<NpcController> PoliceNpcList = new List<NpcController>();
    public List<NpcController> CivilianNpcList = new List<NpcController>();

    public Text ResistanceText;
    public Text PoliceText;
    public Text CivilianText;

    public Color CivilianColor;
    public Color ResistanceColor;
    public Color PoliceColor;

    public Mesh[] CivilianMeshes;
    public Mesh[] PoliceMeshes;
    public Mesh[] ResistanceMeshes;

    public int ResistanceNpcLimit;
    public int CivilianNpcLimit;
    public int PoliceNpcLimit;

    public bool SpawnedResistanceFollowsPlayer;

    public Vector2 ResistanceNpcsCenter
    {
        get
        {
            var center = Vector3.zero;
            for (var i = ResistanceNpcList.Count - 1; i >= 0; i--)
            {
                center += ResistanceNpcList[i].transform.position;
            }
            return center / ResistanceNpcList.Count;
        }
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (!PoliceText)
            PoliceText = GameObject.Find("Police Text").GetComponent<Text>();
        if (!ResistanceText)
            ResistanceText = GameObject.Find("Resistance Text").GetComponent<Text>();
        if (!CivilianText)
            CivilianText = GameObject.Find("Civilian Text").GetComponent<Text>();
    }

    void Update()
    {
        PoliceText.text = "Police: " + PoliceNpcList.Count;
        ResistanceText.text = "Resistance: " + ResistanceNpcList.Count;
        CivilianText.text = "Civilian: " + CivilianNpcList.Count;
    }

    public void ChangeRole(NpcController npc, Role newRole)
    {
        GetRoleList(npc.NpcRole).Remove(npc);
        npc.NpcRole = newRole;
        GetRoleList(newRole).Add(npc);

        npc.Sprite.color = GetRoleColor(newRole);
        npc.gameObject.layer = GetRoleLayer(newRole);

        if (npc.NpcRole == Role.Resistance)
            npc.tag = "Untagged";
        if (newRole == Role.Resistance)
            npc.tag = "Resistance";

        npc.NpcRole = newRole;
        npc.Mesh.mesh = GetRoleMesh(npc.NpcRole);
    }

    private Mesh GetRoleMesh(Role npcNpcRole)
    {
        switch (npcNpcRole)
        {
            case Role.Civillian:
                return CivilianMeshes[Random.Range(0, CivilianMeshes.Length)];
            case Role.Police:
                return PoliceMeshes[Random.Range(0, PoliceMeshes.Length)];
            case Role.Resistance:
                return ResistanceMeshes[Random.Range(0, ResistanceMeshes.Length)];
            default:
                return CivilianMeshes[Random.Range(0, CivilianMeshes.Length)];
        }
    }

    private AttackMode GetRoleAttackMode(Role role)
    {
        if (role == Role.Civillian) return AttackMode.Melee;
        var chance = Random.value;
        if (role == Role.Police)
        {
            var policeCount = PoliceNpcList.Count;
            var resistanceCount = ResistanceNpcList.Count;
            if (chance < .5f)
                return AttackMode.Melee;
            if (chance < .8f && policeCount < resistanceCount)
                return AttackMode.Smoke;
            if (chance <= 1 && policeCount * 2 < resistanceCount)
                return AttackMode.Shoot;
        }
        else
        {
            if (chance < .65f)
                return AttackMode.Melee;
            if (chance < .95f)
                return AttackMode.Molotov;
            if (chance <= 1)
                return AttackMode.Shoot;
        }
        return AttackMode.Melee;

    }
    private int GetRoleLayer(Role role)
    {
        switch (role)
        {
            case Role.Resistance:
                return LayerMask.NameToLayer("Resistance");
            case Role.Civillian:
                return LayerMask.NameToLayer("Civillian");
            case Role.Police:
                return LayerMask.NameToLayer("Police");
            default:
                return LayerMask.NameToLayer("Default");

        }
    }

    private int GetRoleLimit(Role role)
    {
        switch (role)
        {
            case Role.Resistance:
                return ResistanceNpcLimit;
            case Role.Civillian:
                return CivilianNpcLimit;
            case Role.Police:
                return ResistanceNpcList.Count * 3;
            default:
                throw new ArgumentException("Invalid role: " + role);

        }
    }

    private bool IsNpcLimitReachedFor(Role role)
    {
        var list = GetRoleList(role);
        var limit = GetRoleLimit(role);

        return list.Count >= limit;
    }

    public void SpawnNpc(Role role, Vector3 position, Transform follow = null)
    {

        if (IsNpcLimitReachedFor(role))
        {
            return;
        }

        var npc = Instantiate(NpcPrefab, position, Quaternion.identity).GetComponent<NpcController>();
        npc.NpcRole = role;
        npc.AttackMode = GetRoleAttackMode(role);
        npc.Follow.FollowTarget = follow;

        if (SpawnedResistanceFollowsPlayer && role == Role.Resistance)
        {
            npc.Follow.FollowTarget = GameManager.Instance.PlayerTransform;
        }

        GetRoleList(role).Add(npc);

        StatsManager.Instance.IncreaseSpawns(role);
    }

    public void RemoveFromList(NpcController npc)
    {
        GetRoleList(npc.NpcRole).Remove(npc);
    }

    public void DespawnNpc(NpcController npc)
    {
        GetRoleList(npc.NpcRole).Remove(npc);
        Destroy(npc.gameObject);
    }

    public List<NpcController> GetRoleList(Role role)
    {
        switch (role)
        {
            case Role.Resistance:
                return ResistanceNpcList;
            case Role.Police:
                return PoliceNpcList;
            case Role.Civillian:
                return CivilianNpcList;
            default:
                throw new ArgumentException("Invalid role: " + role);
        }
    }

    public Color GetRoleColor(Role role)
    {
        switch (role)
        {
            case Role.Civillian:
                return CivilianColor;
            case Role.Police:
                return PoliceColor;
            case Role.Resistance:
                return ResistanceColor;
            default:
                throw new ArgumentException("Invalid role: " + role);
        }
    }
}