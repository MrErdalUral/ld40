﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Role
{
    Civillian,
    Resistance,
    Police
}
public enum Action
{
    Idle,
    Follow,
    Attack,
    Flee
}
public enum AttackMode
{
    Passive,
    Melee,
    Shoot,
    Molotov,
    Smoke
}
[RequireComponent(typeof(NpcFollow))]
[RequireComponent(typeof(NpcShoot))]
[RequireComponent(typeof(NpcMelee))]
public class NpcController : MonoBehaviour
{

    public SpriteRenderer Sprite;

    public Role NpcRole;
    public Action CurrentAction = Action.Idle;
    public AttackMode AttackMode = AttackMode.Shoot;

    public float IdleCheckInterval;
    [Tooltip("If the NPC has moved less than distance during the specified IdleCheckInterval, consider it idle.")]
    public float IdleDistanceLimit;

    public NpcFollow Follow;
    public PlayerMove Move;
    public NpcShoot Shoot;
    public NpcHealth Health;
    public NpcMelee Melee;

    public float ConvertTime = 3f;
    private bool _interruptConvert;
    private bool _idleWalking;
    private Vector2 _randomDirection;
    public Transform FleeTransform;

    public MeshFilter Mesh;

    // Use this for initialization
    void Start()
    {
        _idleWalking = false;

        if (!Sprite)
            Sprite = GetComponentInChildren<SpriteRenderer>();
        if (!Follow)
            Follow = GetComponent<NpcFollow>();
        if (!Move)
            Move = GetComponent<PlayerMove>();
        if (!Shoot)
            Shoot = GetComponent<NpcShoot>();
        if (!Health)
            Health = GetComponent<NpcHealth>();
        if (!Mesh)
            Mesh = GetComponentInChildren<MeshFilter>();

        NpcManager.Instance.ChangeRole(this, NpcRole);

        StartCoroutine(IdleCheckCoroutine());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (CurrentAction == Action.Follow)
        {
            Follow.Follow();
        }
        if (CurrentAction == Action.Idle)
        {
            if (NpcRole == Role.Resistance && Random.value > .5f)
                Follow.FollowTarget = GameManager.Instance.PlayerTransform;
            IdleMove();
        }
        if (CurrentAction == Action.Attack)
        {
            switch (AttackMode)
            {
                case AttackMode.Shoot:
                    Shoot.Shoot();
                    break;
                case AttackMode.Melee:
                    Melee.Melee();
                    break;
                case AttackMode.Smoke:
                    Shoot.ShootSmoke();
                    break;
                case AttackMode.Molotov:
                    Shoot.ShootMolotov();
                    break;
                case AttackMode.Passive:
                    break;
            }
        }
        if (CurrentAction == Action.Flee && FleeTransform != null)
        {
            var fleeDir = transform.position - FleeTransform.position;
            Move.Move(fleeDir);
        }

    }

    private void IdleMove()
    {
        if (_idleWalking == false)
            StartCoroutine(IdleRandomDirection());
        Move.SlowMove(_randomDirection);
    }

    private IEnumerator IdleRandomDirection()
    {
        _idleWalking = true;
        _randomDirection = new Vector2(Random.Range(-16, 16), Random.Range(-16, 16));
        yield return new WaitForSeconds(Random.Range(3, 5));
        _randomDirection = Vector2.zero;
        yield return new WaitForSeconds(Random.Range(1, 3));
        _idleWalking = false;
    }

    void Update()
    {
        if (CurrentAction == Action.Flee) return;
        else if (Melee.IsTargeting && NpcRole != Role.Civillian)
            CurrentAction = Action.Attack;
        else if (Follow.FollowTarget != null)
            CurrentAction = Action.Follow;
        else
            CurrentAction = Action.Idle;
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        _interruptConvert = false;
        if (NpcRole == Role.Civillian && collision.gameObject.tag == "Player")
        {
            _interruptConvert = false;
            StartCoroutine(ConvertToResistance(collision.gameObject.transform));
        }
        else if (Health && collision.gameObject.tag == "Bullet")
        {
            Destroy(collision.gameObject);
            Health.TakeDamage(3, AttackMode.Shoot); //0 means Bullet damage
        }
        else if (Melee && AttackMode == AttackMode.Melee && ((collision.gameObject.tag == "Police" && NpcRole == Role.Resistance) || (collision.gameObject.tag == "Resistance" && NpcRole == Role.Police)))
        {
            CurrentAction = Action.Attack;
            Melee.MeleeTarget = collision.transform;
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (NpcRole == Role.Civillian && collision.gameObject.tag == "Player")
            _interruptConvert = true;

    }

    private IEnumerator ConvertToResistance(Transform collision)
    {
        yield return new WaitForSeconds(ConvertTime);
        if (_interruptConvert)
            yield break;
        Follow.FollowTarget = collision;

        NpcManager.Instance.ChangeRole(this, Role.Resistance);
    }

    private IEnumerator IdleCheckCoroutine()
    {
        while (true)
        {
            while (CurrentAction == Action.Idle || Follow.IsFollowingPlayer)
                yield return Wait.ForSeconds1;

            var timeInitial = Time.time;
            var positionInitial = transform.position;

            while (timeInitial + IdleCheckInterval > Time.time)
                yield return Wait.ForEndOfFrame;

            var distanceTravelledSqr = (transform.position - positionInitial).sqrMagnitude;

            if (distanceTravelledSqr < IdleDistanceLimit)
            {
                FollowTransform.RemoveFollower(Follow.FollowTarget, this);
                Follow.FollowTarget = null;
                CurrentAction = Action.Idle;
            }
        }
    }

    private void OnDestroy()
    {
        FollowTransform.RemoveFollower(Follow.FollowTarget, this);
        NpcManager.Instance.RemoveFromList(this);
    }
    public void DropModel()
    {
        Mesh.transform.parent.parent = null;
        Mesh.transform.rotation = Quaternion.AngleAxis(-90, Vector3.forward);
        Mesh.transform.parent.rotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward);
        Mesh.GetComponent<MeshRenderer>().material.color /= 2;
    }
}