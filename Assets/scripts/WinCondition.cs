﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Building))]
public class WinCondition : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

		if(GetComponent<Building>().Role == Role.Resistance)
            GameManager.Instance.Win();
	}
}
