﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NpcFollow))]
public class NpcMelee : MonoBehaviour
{
    public LayerMask ObstacleMask;

    public Transform MeleeTarget;
    public float MeleeDamage = 3;
    public float MeleeRange = 16;

    public AudioClip Audio;

    public NpcFollow Follow;
    public List<Transform> TargetList;

    private bool _onCooldown;

    public bool IsTargeting
    {
        get { return TargetList != null && TargetList.Count > 0; }
        set { }
    }

    void Awake()
    {
        _onCooldown = false;
        TargetList = new List<Transform>();
        if (!Follow)
            Follow = GetComponent<NpcFollow>();

        StartCoroutine(KeepTargetListCleanCoroutine());
    }
    private Transform FindTarget()
    {
        if (TargetList.Count < 1) return null;
        var index = Random.Range(0, TargetList.Count);
        var target = TargetList[index];
        if (target == null)
        {
            TargetList.RemoveAt(index);
            return null;
        }
        RaycastHit2D line = Physics2D.Linecast(transform.position, target.position, ObstacleMask);
        if (line.transform != null)
            return null;
        else
            return target;
    }
    private IEnumerator KeepTargetListCleanCoroutine()
    {
        while (isActiveAndEnabled)
        {
            for (int i = TargetList.Count - 1; i >= 0; i--)
            {
                if (TargetList[i] == null)
                    TargetList.RemoveAt(i);
            }
            yield return Wait.ForSeconds1;
        }
    }

    private IEnumerator Cooldown()
    {
        _onCooldown = true;
        yield return new WaitForSeconds(2f);
        _onCooldown = false;
    }
    public void Melee()
    {
        if (TargetList.Count <= 0)
        {
            return;
        }
        if (MeleeTarget == null)
        {
            MeleeTarget = FindTarget();
        }
        if (MeleeTarget == null)
        {
            Follow.FollowTarget = GameManager.Instance.PlayerTransform;
            Follow.Follow();
            return;
        }
        var distance = (transform.position - MeleeTarget.position).magnitude;

        if (distance > MeleeRange)
        {
            Follow.FollowTarget = MeleeTarget;
            Follow.Follow();
        }
        else if (!_onCooldown)
        {
            var npcHealth = MeleeTarget.GetComponent<NpcHealth>();
            if (npcHealth)
            {
                npcHealth.TakeDamage(MeleeDamage, AttackMode.Melee);
                AudioManager.Instance.Play(Audio, maxPitchOffset: 0.15f, at: transform);
                StartCoroutine(Cooldown());
            }
        }
    }
}
