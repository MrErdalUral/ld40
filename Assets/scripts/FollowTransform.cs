using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    private bool DontDestroy
    {
        get { return name == "FollowTransform"; }
    }

    private List<NpcController> _followers;

    private static readonly Dictionary<Vector2, FollowTransform> _transforms = new Dictionary<Vector2, FollowTransform>();
    private static readonly Dictionary<NpcController, FollowTransform> _npcToFollowTransform = new Dictionary<NpcController, FollowTransform>();

    private static GameObject _gameObject;

    private float _dontDestroyUntil;

    private void Awake()
    {
        _followers = new List<NpcController>();
        _transforms.Add(transform.position, this);
        // in order to wait for an npc to be added to _followers
        _dontDestroyUntil = Time.time + 1;
    }

    private void Update()
    {
        if (!DontDestroy && _dontDestroyUntil < Time.time && _followers.Count == 0)
        {
            _transforms.Remove(transform.position);
            Destroy(gameObject);
        }
    }

    //
    // Summary:
    //     ///
    //     Creates a stationary copy of the given transform and makes the specified npc follow it.
    //     ///
    public static Transform AddFollower(Transform transform, NpcController npc)
    {
        var followTransform = GetFollowTransformOf(transform, true);
        followTransform._followers.Add(npc);

        if (_npcToFollowTransform.ContainsKey(npc))
            _npcToFollowTransform[npc] = followTransform;
        else
            _npcToFollowTransform.Add(npc, followTransform);

        return followTransform.transform;
    }

    public static void RemoveFollower(FollowTransform transform, NpcController npc)
    {
        if (transform != null)
            RemoveFollower(transform.transform, npc);
    }

    public static void RemoveFollower(Transform transform, NpcController npc)
    {
        var followTransform = GetFollowTransformOf(transform);
        if (followTransform != null)
            followTransform._followers.Remove(npc);


    }

    public static void RemoveFollower(NpcController npc)
    {
        FollowTransform followTransform;
        if (_npcToFollowTransform.TryGetValue(npc, out followTransform))
            RemoveFollower(followTransform, npc);
    }

    public static FollowTransform GetFollowTransformOf(Transform transform, bool createIfNotExists = false)
    {
        if (transform == null)
            return null;

        FollowTransform followTransform;
        if (_transforms.TryGetValue(transform.position, out followTransform))
            return followTransform;

        if (createIfNotExists && !_gameObject)
        {
            var go = new GameObject("FollowTransform");
            go.AddComponent<FollowTransform>();
            _gameObject = go;
        }

        return createIfNotExists
            ? Instantiate(_gameObject, transform.position, transform.rotation).GetComponent<FollowTransform>()
            : null;
    }
}