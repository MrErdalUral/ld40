﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform PlayerTransform;
    public PlayerCommandController Command;

    void Start()
    {
        PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        Command = PlayerTransform.GetComponent<PlayerCommandController>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Command != null && Command._commandSprite.enabled)
        {
            transform.position = Vector3.Lerp(transform.position, Command.CommandTarget.position + new Vector3(0, 0, transform.position.z), Time.deltaTime*5);
            return;
        }
        if (PlayerTransform == null) return;
        transform.position = Vector3.Lerp(transform.position, PlayerTransform.position + new Vector3(0, 0, transform.position.z), Time.deltaTime);
    }
}
