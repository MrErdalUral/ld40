using System.Collections;
using UnityEngine;

public abstract class FadeOutObject : MonoBehaviour
{
    public float FadeDelay;
    public float FadeDuration;

    private void Start()
    {
        StartCoroutine(FadeOut());
        Destroy(gameObject, FadeDelay + FadeDuration);
    }

    public IEnumerator FadeOut()
    {
        var t0 = Time.time;
        while (Time.time - t0 < FadeDelay)
        {
            yield return Wait.ForEndOfFrame;
        }

        t0 = Time.time;
        while (Time.time - t0 < FadeDuration)
        {
            Fade();
            yield return Wait.ForEndOfFrame;
        }
    }

    public virtual void Fade() {

    }
}