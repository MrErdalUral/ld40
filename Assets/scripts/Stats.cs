using UnityEngine;

[System.Serializable]
public class Stats
{
    public int ResistanceSpawns;
    public int CivilianSpawns;
    public int PoliceSpawns;

    public int CasualtiesTotal
    {
        get
        {
            var byRole = CivilianCasualties + ResistanceCasualties + PoliceCasualties;
            var byMeans = GunCasualties + MeleeCasualties + SmokeCasualties + MolotovCasualties;

            Debug.Log("Casualties by role: " + (byRole));
            Debug.Log("Casualties by means: " + (byMeans));

            return byRole;
        }
    }

    public int MolotovCasualties;
    public int SmokeCasualties;
    public int MeleeCasualties;
    public int GunCasualties;
    public int PoliceCasualties;
    public int CivilianCasualties;
    public int ResistanceCasualties;

    public int CapturedBuildingResistance;
    public int CapturedBuildingPolice;
}