using System;
using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Tooltip("Wait until spawn.")]
    public float SpawnRateResistance;
    [Tooltip("Wait until spawn.")]
    public float SpawnRateCivilian;
    [Tooltip("Wait until spawn.")]
    public float SpawnRatePolice;

    public Role Role
    {
        get
        {
            return Building.Role;
        }
    }

    public float RoleSpawnRate
    {
        get
        {
            switch (Role)
            {
                case Role.Resistance:
                    return SpawnRateResistance;
                case Role.Civillian:
                    return SpawnRateCivilian;
                case Role.Police:
                    return SpawnRatePolice;
                default:
                    throw new ArgumentException("Invalid role.");
            }
        }
    }

    private Door _door;
    public Door Door
    {
        get
        {
            if (_door == null)
                _door = Building.Door;
            return _door;
        }
        set
        {
            _door = value;
        }
    }

    [HideInInspector]
    public Building Building;

    private float _lastSpawnTime;

    private void Awake()
    {
        Building = GetComponentInParent<Building>();
        Door = Building.Door;
    }

    private void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            if (!isActiveAndEnabled)
                yield break;

            var waitTime = RoleSpawnRate;
            while (Time.time - _lastSpawnTime < RoleSpawnRate)
            {
                yield return Wait.ForEndOfFrame;
            }

            if (Role == Role.Civillian)
                NpcManager.Instance.SpawnNpc(Role, Door.transform.position);
            else if (Building.NpcCount < Building.NpcLimit)
                Building.NpcCount++;
            else
                NpcManager.Instance.SpawnNpc(Role, Door.transform.position);

            ResetTimer();
        }
    }

    public void ResetTimer()
    {
        _lastSpawnTime = Time.time;
    }
}