﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{

    public AudioClip Audio;

    public float DestroyTime = 2f;
    void Start()
    {
        AudioManager.Instance.Play(Audio, maxPitchOffset: .15f, at: transform);
        Destroy(gameObject,DestroyTime);
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
