﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMove))]
public class NpcFollow : MonoBehaviour
{
    public bool IsFollowingPlayer
    {
        get { return FollowTarget == GameManager.Instance.PlayerTransform; }
    }
    public Transform FollowTarget;
    public PlayerMove Move;
    public float MinimumDistanceToTarget = 10f;
    void Awake()
    {
        if (!Move)
            Move = GetComponent<PlayerMove>();
    }
    // Update is called once per frame
    public void Follow()
    {
        if (!FollowTarget) return;
        if ((transform.position - FollowTarget.position).magnitude < MinimumDistanceToTarget) return;
        var v = FollowTarget.position - transform.position;
        Move.Move(v);
    }
}
