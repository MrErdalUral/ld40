using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    [HideInInspector]
    public Door Door;

    [HideInInspector]
    public Spawner Spawner;

    [HideInInspector]
    public SpriteRenderer Sprite;

    public Role Role;

    [Tooltip("NPCs required to capture the building")]
    public int NpcLimit;

    public int NpcCount
    {
        get
        {
            if (Role == Role.Resistance)
                return NpcCountResistance;
            else if (Role == Role.Police)
                return NpcCountPolice;
            return 0;
        }
        set
        {
            if (Role == Role.Resistance)
                NpcCountResistance = value;
            else if (Role == Role.Police)
                NpcCountPolice = value;
        }
    }
    public int NpcCountOther
    {
        get
        {
            if (Role == Role.Resistance)
                return NpcCountPolice;
            else if (Role == Role.Police)
                return NpcCountResistance;
            return 0;
        }
        set
        {
            if (Role == Role.Resistance)
                NpcCountPolice = value;
            else if (Role == Role.Police)
                NpcCountResistance = value;
        }
    }

    public int NpcCountResistance;
    public int NpcCountPolice;

    public bool IsCapturable;
    public bool IsCaptured;

    public static List<Building> Buildings = new List<Building>();
    private static Collider2D[] _colliders = new Collider2D[1]; // for overlapPointNonAlloc

    private static LayerMask _buildingLayer;

    private MeshRenderer _meshRenderer;

    private Color _colorStep = new Color(.3f, 0, 0);
    private bool _isBlinking;

    private Color _initialMeshColor;
    private Color _initialSpriteColor;

    private void Awake()
    {
        Buildings.Add(this);
        Door = GetComponentInChildren<Door>();
        Spawner = GetComponentInChildren<Spawner>();
        Sprite = GetComponentInChildren<SpriteRenderer>();
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        _buildingLayer = LayerMask.GetMask("Building");
        _initialMeshColor = _meshRenderer.material.color;
        _initialSpriteColor = Sprite.color;
    }

    public static bool IsBuilding(Vector2 v)
    {
        for (int i = Buildings.Count - 1; i >= 0; i--)
        {
            var colliderCount = Physics2D.OverlapPointNonAlloc(v, _colliders, _buildingLayer);
            if (colliderCount == 1)
            {
                return true;
            }
            if (colliderCount > 1)
            {
                var colliders = Physics2D.OverlapPointAll(v);
                for (int j = 0; j < colliders.Length; j++)
                {
                    print(colliders[i]);
                }
                throw new System.Exception("Multiple colliders");
            }
        }
        return false;
    }

    public static bool TryGetBuildingAt(Vector2 v, out Building building)
    {
        for (int i = Buildings.Count - 1; i >= 0; i--)
        {
            var colliderCount = Physics2D.OverlapPointNonAlloc(v, _colliders, _buildingLayer);
            if (colliderCount == 1)
            {
                building = _colliders[0].GetComponentInParent<Building>();
                return true;
            }
            if (colliderCount > 1)
            {
                var colliders = Physics2D.OverlapPointAll(v);
                for (int j = 0; j < colliders.Length; j++)
                {
                    print(colliders[i]);
                }
                throw new System.Exception("Multiple colliders");
            }
        }
        building = null;
        return false;
    }

    private bool CheckCapture()
    {
        if (NpcCount == 0 && Role != Role.Civillian)
        {
            Role = Role.Civillian;
            Sprite.color = _initialSpriteColor;
            _meshRenderer.material.color = _initialMeshColor;
            return false;
        }

        if (NpcLimit == NpcCountResistance && Role != Role.Resistance)
            Role = Role.Resistance;
        else if (NpcLimit == NpcCountPolice && Role != Role.Police)
            Role = Role.Police;
        else
            return false;

        // building is captured
        Sprite.color = NpcManager.Instance.GetRoleColor(Role);
        Spawner.ResetTimer();

        StatsManager.Instance.IncreaseBuildingCaptures(Role);

        return true;
    }

    public bool Enter(NpcController npc)
    {
        if (!IsCapturable)
            return false;

        if (npc.NpcRole == Role.Civillian)
            return false;

        switch(npc.NpcRole)
        {
            case Role.Resistance:
                if (NpcCountPolice > 0)
                {
                    NpcCountPolice--;
                    SubtractColor(Role.Police);
                }
                else if (NpcCountResistance < NpcLimit)
                {
                    NpcCountResistance++;
                    AddColor(Role.Resistance);
                }
                else
                {
                    return false; // building is full, should return false?
                }
                break;

            case Role.Police:
                if (NpcCountResistance > 0)
                {
                    NpcCountResistance--;
                    SubtractColor(Role.Resistance);
                }
                else if (NpcCountPolice < NpcLimit)
                {
                    NpcCountPolice++;
                    AddColor(Role.Police);
                }
                else
                {
                    return false; // building is full, should return false?
                }
                break;
        }

        NpcManager.Instance.DespawnNpc(npc);
        CheckCapture();

        return true;
    }

    public void Blink(Role role = Role.Civillian)
    {
        StartCoroutine(BlinkCoroutine(role));
    }

    private IEnumerator BlinkCoroutine(Role role)
    {
        if (_isBlinking)
            yield break;

        var color = NpcManager.Instance.GetRoleColor(role);

        _isBlinking = true;

        var t0 = Time.time;
        while (Time.time - t0 < .2f)
        {
            _meshRenderer.material.color += color;
            yield return Wait.ForEndOfFrame;
        }

        t0 = Time.time;
        while (Time.time - t0 < .2f)
        {
            _meshRenderer.material.color -= color;
            yield return Wait.ForEndOfFrame;
        }

        _meshRenderer.material.color = _initialMeshColor;
        _isBlinking = false;
    }

    private void AddColor(Role role)
    {
        var color = NpcManager.Instance.GetRoleColor(role);
        _meshRenderer.material.color += color;
    }

    private void SubtractColor(Role role)
    {
        var color = NpcManager.Instance.GetRoleColor(role);
        _meshRenderer.material.color -= color;
    }
}