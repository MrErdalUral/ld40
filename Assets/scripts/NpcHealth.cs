﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BlinkController))]
public class NpcHealth : MonoBehaviour
{
    public AudioPlayer ScreamAudio;
    public AudioPlayer CoughAudio;

    public AudioPlayer GruntAudio;

    public float Health = 3;
    public float FleeTime = 2f;

    private BlinkController _blinkController;

    private void Awake()
    {
        _blinkController = GetComponent<BlinkController>();
    }

    public bool TakeDamage(float damage, AttackMode damageType)
    {
        Health -= damage;

        if (GruntAudio)
            GruntAudio.Play();

        if (_blinkController != null)
            _blinkController.Blink();

        if (Health <= 0)
        {
            var npcController = GetComponent<NpcController>();
            if (npcController) { 
                npcController.DropModel();
                Destroy(gameObject);
                StatsManager.Instance.IncreaseDeath(damageType);
                StatsManager.Instance.IncreaseDeath(npcController.NpcRole);
            }
            return true;
        }
        if (damageType == AttackMode.Molotov || damageType == AttackMode.Smoke)
        {
            if (damageType == AttackMode.Molotov && ScreamAudio)
                ScreamAudio.Play();
            else if (CoughAudio)
                CoughAudio.Play();

            StartCoroutine(Flee());
        }
        return false;
    }

    private IEnumerator Flee()
    {
        var npcController = GetComponent<NpcController>();
        if (!npcController)
            yield break;
        npcController.CurrentAction = Action.Flee;
        yield return new WaitForSeconds(FleeTime);
        npcController.FleeTransform = null;
        npcController.CurrentAction = Action.Idle;
    }
}