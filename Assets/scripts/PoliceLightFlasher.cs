using System.Collections;
using UnityEngine;

public class PoliceLightFlasher : MonoBehaviour
{
    private Light[] _lights;

    public FlashPatternData FlashPatternBlue;
    public FlashPatternData FlashPatternRed;

    private void Awake()
    {
        PrepareLights();

        FlashPatternBlue.Index = 0;
        FlashPatternRed.Index = 1;
        FlashPattern();
    }

    private void PrepareLights()
    {
        _lights = GetComponentsInChildren<Light>(true);

        if (_lights[0].name.Contains("blue"))
            return;

        var blue = _lights[1];
        _lights[1] = _lights[0];
        _lights[0] = blue;
    }

    private void Toggle(int index, bool on)
    {
        _lights[index].gameObject.SetActive(on);
    }

    private void ToggleOther(int index, bool on)
    {
        // index is either 0 or 1
        _lights[index ^ 1].gameObject.SetActive(on);
    }

    private IEnumerator Flash(int index, int times, WaitForSeconds interval, WaitForSeconds after = null, WaitForSeconds delay = null)
    {
        if (delay != null)
            yield return delay;

        while (true)
        {
            for (int i = 0; i < times; i++)
            {
                Toggle(index, true);
                yield return interval;
                Toggle(index, false);
                yield return interval;
            }

            if (after != null)
                yield return after;
        }
    }

    private IEnumerator Flash(FlashPatternData data)
    {
        return Flash(data.Index, data.Times,
            Wait.GetForSeconds(data.Interval),
            Wait.GetForSeconds(data.After),
            Wait.GetForSeconds(data.Delay));
    }

    private void FlashPattern()
    {
        StartCoroutine(Flash(FlashPatternBlue));
        StartCoroutine(Flash(FlashPatternRed));
    }

    [System.Serializable]
    public class FlashPatternData
    {
        public int Index;
        public int Times;
        public float Interval;
        public float After;
        public float Delay;

        public FlashPatternData(int index, int times, float interval, float after, float delay)
        {
            Index = index;
            Times = times;
            Interval = interval;
            After = after;
            Delay = delay;
        }
    }
}