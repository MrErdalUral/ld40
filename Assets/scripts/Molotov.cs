using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Molotov : FadeOutObject
{
    public float DamagePerSecond;

    private SpriteRenderer _sprite;

    private List<NpcHealth> _damageList = new List<NpcHealth>();

    public AudioClip Audio;

    private void Awake()
    {
        _sprite = GetComponentInChildren<SpriteRenderer>();
        AudioManager.Instance.Play(Audio, maxPitchOffset: .15f, at: transform);
    }

    private void Update()
    {
        for (int i = _damageList.Count - 1; i >= 0; i--)
        {
            var health = _damageList[i];
            if (health == null || health.TakeDamage(Time.deltaTime * DamagePerSecond, AttackMode.Molotov))
                _damageList.Remove(_damageList[i]);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var health = other.GetComponent<NpcHealth>();
        if (health != null)
        {
            _damageList.Add(health);
        }
        var controller = other.GetComponent<NpcController>();
        if (controller)
        {
            controller.FleeTransform = transform;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var health = other.GetComponent<NpcHealth>();
        if (health != null)
        {
            _damageList.Remove(health);
        }
        var controller = other.GetComponent<NpcController>();
    }

    public override void Fade()
    {
        FadeSprite();
        DamagePerSecond -= Time.deltaTime / FadeDuration * DamagePerSecond;
    }

    private void FadeSprite()
    {
        var color = _sprite.color;
        color.a -= Time.deltaTime / FadeDuration;
        _sprite.color = color;
    }
}