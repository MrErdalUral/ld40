using System.Collections.Generic;
using UnityEngine;

public class PlayerCommandController : MonoBehaviour
{
    public Transform CommandTarget;

    public float CommandModeTimeScale;

    public float CommandSpeedMultiplier = 1;

    private PlayerController _playerController;
    private PlayerMove _playerMove;
    public SpriteRenderer _commandSprite;

    private Collider2D[] _colliders = new Collider2D[1];

    private LayerMask _playerLayer;
    private LayerMask _commandLayer;
    private LayerMask _resistanceLayer;

    private List<NpcController> _selection = new List<NpcController>();

    private float _initialFixedDeltaTime;

    private Color _selectStep = new Color(.2f, .2f, .2f);
    private Vector2 _size;

    private void Awake()
    {
        _playerController = GetComponent<PlayerController>();
        _playerMove = GetComponent<PlayerMove>();

        _commandSprite = CommandTarget.GetComponent<SpriteRenderer>();

        _playerLayer = LayerMask.GetMask("Player");
        _commandLayer = LayerMask.GetMask("Command");
        _resistanceLayer = LayerMask.GetMask("Resistance");

        var size = GetComponentInChildren<BoxCollider2D>().size;
        var scale = transform.localScale.xy();
        _size = new Vector2(size.x * scale.x, size.y * scale.y);

        _commandSprite.enabled = false;
        _initialFixedDeltaTime = Time.fixedDeltaTime;
    }

    private void ProcessKeyboardInput()
    {
        if (!_playerController.isActiveAndEnabled)
        {
            var horizontal = Input.GetAxisRaw("Horizontal");
            var vertical = Input.GetAxisRaw("Vertical");

            var inputV = new Vector3(horizontal, vertical, 0).normalized * CommandSpeedMultiplier * Time.deltaTime;
            CommandTarget.transform.Translate(inputV);
            CommandTarget.rotation = Quaternion.identity;
        }
    }

    private void Update()
    {
        ProcessKeyboardInput();

        if (Input.GetButtonDown("Fire3"))
        {
            SlowTime();

            CommandStartFollowingPlayer();
            CommandTarget.localPosition = Vector3.zero;

            ClearSelection();
        }
        if (Input.GetButton("Fire3"))
        {
            _commandSprite.enabled = true;
            _playerController.enabled = false;
            _playerMove.Stop();

            var onResistance = Physics2D.OverlapBoxNonAlloc(CommandTarget.position, _size, 0, _colliders, _resistanceLayer) > 0;
            if (onResistance)
            {
                Select(_colliders[0].GetComponentInChildren<NpcController>());
            }

            for (int i = _selection.Count - 1; i >= 0; i--)
            {
                MoveCommand.To(_selection[i], CommandTarget, false, false);
            }
        }
        else if (Input.GetButtonUp("Fire3"))
        {
            _commandSprite.enabled = false;
            _playerController.enabled = true;

            var onPlayer = Physics2D.OverlapPointNonAlloc(transform.position, _colliders, _commandLayer) > 0;
            if (onPlayer)
            {
                for (var i = _selection.Count - 1; i >= 0; i--)
                {
                    MoveCommand.To(_selection[i], transform);
                    Deselect(_selection[i]);
                }
            }
            else
            {
                CommandStopFollowingPlayer();
                for (int i = _selection.Count - 1; i >= 0; i--)
                {
                    MoveCommand.To(_selection[i], CommandTarget);
                    Deselect(_selection[i]);
                }
            }
            ClearSelection();
            NormalTime();
        }
    }

    private void CommandStopFollowingPlayer()
    {
        CommandTarget.parent = null;
        _playerController.enabled = true;
    }

    private void CommandStartFollowingPlayer()
    {
        CommandTarget.parent = transform;
    }

    private void ClearSelection()
    {
        _selection.Clear();
    }

    private void Deselect(NpcController npc)
    {
        if (npc != null && npc.Sprite != null)
            npc.Sprite.color = NpcManager.Instance.ResistanceColor;
    }

    private void Select(NpcController npc)
    {
        if (!_selection.Contains(npc))
        {
            _selection.Add(npc);
            npc.Sprite.color = NpcManager.Instance.ResistanceColor + _selectStep;
        }
    }

    private void SlowTime()
    {
        Time.timeScale = CommandModeTimeScale;
        Time.fixedDeltaTime = _initialFixedDeltaTime * CommandModeTimeScale;
    }

    private void NormalTime()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = _initialFixedDeltaTime;
    }
}