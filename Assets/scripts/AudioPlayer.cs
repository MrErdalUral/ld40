using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public AudioClip[] Clips;

    public float Volume = 1;
    public float MaxPitchOffset;
    public float Delay;

    public bool Loop;

    public bool PlayInstantly;

    private AudioSource _audioSource;

    private void Awake()
    {
        if (PlayInstantly)
        {
            _audioSource = gameObject.AddComponent<AudioSource>();
        }
    }

    private void Start()
    {
        if (PlayInstantly)
        {
            _audioSource.clip = Clips.SelectRandom();
            _audioSource.Play();
        }
    }

    public void Play()
    {
        var clip = Clips.SelectRandom();
        AudioManager.Instance.Play(clip, Volume, Loop, MaxPitchOffset, Delay, transform);
    }
}