﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMove))]
[RequireComponent(typeof(NpcHealth))]
public class PlayerController : MonoBehaviour
{
    public PlayerMove PlayerMove;
    public NpcHealth NpcHealth;
    // Use this for initialization
    void Start()
    {
        if (!PlayerMove)
            PlayerMove = GetComponent<PlayerMove>();
        if (!NpcHealth)
            NpcHealth = GetComponent<NpcHealth>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var horizontal = Input.GetAxisRaw("Horizontal");
        var vertical = Input.GetAxisRaw("Vertical");
        var inputV = new Vector2(horizontal, vertical);

        PlayerMove.Move(inputV);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            NpcHealth.TakeDamage(3, AttackMode.Shoot);
        }
    }
}
