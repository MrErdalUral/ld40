using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [HideInInspector]
    public Transform PlayerTransform;

    [HideInInspector]
    public AudioManager AudioManager;

    private bool _isGameEnded;

    private void Awake()
    {
        _isGameEnded = false;
        Instance = this;
    }

    private void Start()
    {
        Helpers.AssignIfNull(ref AudioManager);
        PlayerTransform = UnityEngine.GameObject.FindWithTag("Player").transform;
    }

    public void Win()
    {
        if (_isGameEnded) return;
        Debug.Log("You Win!");
        _isGameEnded = true;
        SceneManager.LoadScene("win");
    }
    public void Lose()
    {
        if (_isGameEnded) return;
        Debug.Log("You Lose!");
        _isGameEnded = true;
        StartCoroutine(LoseRoutine());

    }

    private IEnumerator LoseRoutine()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("lose");

    }
}