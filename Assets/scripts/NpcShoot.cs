﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NpcController))]
public class NpcShoot : MonoBehaviour
{
    public LayerMask ObstacleMask;
    public Rigidbody2D PoliceProjectilePrefab;
    public Rigidbody2D ResistanceProjectilePrefab;
    public Rigidbody2D MolotovProjectilePrefab;
    public Rigidbody2D SmokeProjectilePrefab;

    public Rigidbody2D ProjectilePrefab
    {
        get { return GetComponent<NpcController>().NpcRole == Role.Resistance ? ResistanceProjectilePrefab : PoliceProjectilePrefab; }
        set { }
    }
    public float DestroyTime = 2f;
    public float Speed = 1000f;
    public List<Transform> TargetList;
    private bool _onCooldown;
    void Awake()
    {
        _onCooldown = false;
        TargetList = new List<Transform>();
    }

    public void Shoot()
    {
        if (_onCooldown) return;
        if (!ProjectilePrefab) return;
        Vector2 targetPos = FindTarget();
        if (targetPos == Vector2.zero) return;
        var instance = GameObject.Instantiate(ProjectilePrefab, transform.position, Quaternion.identity);
        var direction = targetPos - (Vector2)transform.position;
        instance.velocity = (direction.normalized + new Vector2(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f))).normalized * Speed;
        //Destroy(instance.gameObject, DestroyTime);
        StartCoroutine(Cooldown(1));
    }

    public void ShootMolotov()
    {
        if (_onCooldown) return;
        if (!ProjectilePrefab) return;
        Vector2 targetPos = FindTarget();
        if (targetPos == Vector2.zero) return;
        var instance = GameObject.Instantiate(MolotovProjectilePrefab, transform.position, Quaternion.identity);
        var direction = targetPos - (Vector2)transform.position;
        instance.velocity = (direction.normalized + new Vector2(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f))).normalized * Speed / 4;
        //Destroy(instance.gameObject, DestroyTime);
        GetComponent<NpcController>().AttackMode = AttackMode.Melee;
        StartCoroutine(Cooldown(5));
    }
    public void ShootSmoke()
    {
        if (_onCooldown) return;
        if (!ProjectilePrefab) return;
        Vector2 targetPos = FindTarget();
        if (targetPos == Vector2.zero) return;
        var instance = GameObject.Instantiate(SmokeProjectilePrefab, transform.position, Quaternion.identity);
        var direction = targetPos - (Vector2)transform.position;
        instance.velocity = (direction.normalized + new Vector2(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f))).normalized * Speed / 4;
        //Destroy(instance.gameObject, DestroyTime);
        StartCoroutine(Cooldown(5));
    }


    private IEnumerator Cooldown(float t)
    {
        _onCooldown = true;
        yield return new WaitForSeconds(t);
        _onCooldown = false;
    }

    private Vector2 FindTarget()
    {
        if (TargetList.Count < 1) return Vector2.zero;
        var index = Random.Range(0, TargetList.Count);
        var target = TargetList[index];
        if (target == null)
        {
            TargetList.RemoveAt(index);
            return Vector2.zero;
        }
        RaycastHit2D line = Physics2D.Linecast(transform.position, target.position, ObstacleMask);
        if (line.transform != null)
            return Vector2.zero;
        else
            return target.position;
    }
}
