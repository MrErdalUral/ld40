using UnityEngine;

public static class Wait
{
    public static readonly WaitForEndOfFrame ForEndOfFrame = new WaitForEndOfFrame();
    public static readonly WaitForFixedUpdate ForFixedUpdate = new WaitForFixedUpdate();
    public static readonly WaitForSecondsRealtime ForSecondsRealtime1 = new WaitForSecondsRealtime(1);
    public static readonly WaitForSeconds ForSeconds0 = new WaitForSeconds(0);
    public static readonly WaitForSeconds ForSeconds1 = new WaitForSeconds(1);
    public static readonly WaitForSeconds ForSeconds2 = new WaitForSeconds(2);

    #region WaitForSeconds steps getter
    // for t in [0, 2], in steps of 0.05
    public static WaitForSeconds GetForSeconds(float t)
    {
        if (Mathf.Approximately(t, 0f)) return ForSeconds0;
        if (Mathf.Approximately(t, 1f)) return ForSeconds1;
        if (Mathf.Approximately(t, 2f)) return ForSeconds2;

        if (Mathf.Approximately(t, 0.05f)) return ForSeconds0_05;
        if (Mathf.Approximately(t, 0.1f)) return ForSeconds0_1;
        if (Mathf.Approximately(t, 0.15f)) return ForSeconds0_15;
        if (Mathf.Approximately(t, 0.2f)) return ForSeconds0_2;
        if (Mathf.Approximately(t, 0.25f)) return ForSeconds0_25;
        if (Mathf.Approximately(t, 0.3f)) return ForSeconds0_3;
        if (Mathf.Approximately(t, 0.35f)) return ForSeconds0_35;
        if (Mathf.Approximately(t, 0.4f)) return ForSeconds0_4;
        if (Mathf.Approximately(t, 0.45f)) return ForSeconds0_45;
        if (Mathf.Approximately(t, 0.5f)) return ForSeconds0_5;
        if (Mathf.Approximately(t, 0.55f)) return ForSeconds0_55;
        if (Mathf.Approximately(t, 0.6f)) return ForSeconds0_6;
        if (Mathf.Approximately(t, 0.65f)) return ForSeconds0_65;
        if (Mathf.Approximately(t, 0.7f)) return ForSeconds0_7;
        if (Mathf.Approximately(t, 0.75f)) return ForSeconds0_75;
        if (Mathf.Approximately(t, 0.8f)) return ForSeconds0_8;
        if (Mathf.Approximately(t, 0.85f)) return ForSeconds0_85;
        if (Mathf.Approximately(t, 0.9f)) return ForSeconds0_9;
        if (Mathf.Approximately(t, 0.95f)) return ForSeconds0_95;
        if (Mathf.Approximately(t, 1.05f)) return ForSeconds1_05;
        if (Mathf.Approximately(t, 1.1f)) return ForSeconds1_1;
        if (Mathf.Approximately(t, 1.15f)) return ForSeconds1_15;
        if (Mathf.Approximately(t, 1.2f)) return ForSeconds1_2;
        if (Mathf.Approximately(t, 1.25f)) return ForSeconds1_25;
        if (Mathf.Approximately(t, 1.3f)) return ForSeconds1_3;
        if (Mathf.Approximately(t, 1.35f)) return ForSeconds1_35;
        if (Mathf.Approximately(t, 1.4f)) return ForSeconds1_4;
        if (Mathf.Approximately(t, 1.45f)) return ForSeconds1_45;
        if (Mathf.Approximately(t, 1.5f)) return ForSeconds1_5;
        if (Mathf.Approximately(t, 1.55f)) return ForSeconds1_55;
        if (Mathf.Approximately(t, 1.6f)) return ForSeconds1_6;
        if (Mathf.Approximately(t, 1.65f)) return ForSeconds1_65;
        if (Mathf.Approximately(t, 1.7f)) return ForSeconds1_7;
        if (Mathf.Approximately(t, 1.75f)) return ForSeconds1_75;
        if (Mathf.Approximately(t, 1.8f)) return ForSeconds1_8;
        if (Mathf.Approximately(t, 1.85f)) return ForSeconds1_85;
        if (Mathf.Approximately(t, 1.9f)) return ForSeconds1_9;
        if (Mathf.Approximately(t, 1.95f)) return ForSeconds1_95;
        return null;
    }
    #endregion

    #region WaitForSeconds steps
    public static readonly WaitForSeconds ForSeconds0_05 = new WaitForSeconds(0.05f);
    public static readonly WaitForSeconds ForSeconds0_1 = new WaitForSeconds(0.1f);
    public static readonly WaitForSeconds ForSeconds0_15 = new WaitForSeconds(0.15f);
    public static readonly WaitForSeconds ForSeconds0_2 = new WaitForSeconds(0.2f);
    public static readonly WaitForSeconds ForSeconds0_25 = new WaitForSeconds(0.25f);
    public static readonly WaitForSeconds ForSeconds0_3 = new WaitForSeconds(0.3f);
    public static readonly WaitForSeconds ForSeconds0_35 = new WaitForSeconds(0.35f);
    public static readonly WaitForSeconds ForSeconds0_4 = new WaitForSeconds(0.4f);
    public static readonly WaitForSeconds ForSeconds0_45 = new WaitForSeconds(0.45f);
    public static readonly WaitForSeconds ForSeconds0_5 = new WaitForSeconds(0.5f);
    public static readonly WaitForSeconds ForSeconds0_55 = new WaitForSeconds(0.55f);
    public static readonly WaitForSeconds ForSeconds0_6 = new WaitForSeconds(0.6f);
    public static readonly WaitForSeconds ForSeconds0_65 = new WaitForSeconds(0.65f);
    public static readonly WaitForSeconds ForSeconds0_7 = new WaitForSeconds(0.7f);
    public static readonly WaitForSeconds ForSeconds0_75 = new WaitForSeconds(0.75f);
    public static readonly WaitForSeconds ForSeconds0_8 = new WaitForSeconds(0.8f);
    public static readonly WaitForSeconds ForSeconds0_85 = new WaitForSeconds(0.85f);
    public static readonly WaitForSeconds ForSeconds0_9 = new WaitForSeconds(0.9f);
    public static readonly WaitForSeconds ForSeconds0_95 = new WaitForSeconds(0.95f);

    public static readonly WaitForSeconds ForSeconds1_05 = new WaitForSeconds(1.05f);
    public static readonly WaitForSeconds ForSeconds1_1 = new WaitForSeconds(1.1f);
    public static readonly WaitForSeconds ForSeconds1_15 = new WaitForSeconds(1.15f);
    public static readonly WaitForSeconds ForSeconds1_2 = new WaitForSeconds(1.2f);
    public static readonly WaitForSeconds ForSeconds1_25 = new WaitForSeconds(1.25f);
    public static readonly WaitForSeconds ForSeconds1_3 = new WaitForSeconds(1.3f);
    public static readonly WaitForSeconds ForSeconds1_35 = new WaitForSeconds(1.35f);
    public static readonly WaitForSeconds ForSeconds1_4 = new WaitForSeconds(1.4f);
    public static readonly WaitForSeconds ForSeconds1_45 = new WaitForSeconds(1.45f);
    public static readonly WaitForSeconds ForSeconds1_5 = new WaitForSeconds(1.5f);
    public static readonly WaitForSeconds ForSeconds1_55 = new WaitForSeconds(1.55f);
    public static readonly WaitForSeconds ForSeconds1_6 = new WaitForSeconds(1.6f);
    public static readonly WaitForSeconds ForSeconds1_65 = new WaitForSeconds(1.65f);
    public static readonly WaitForSeconds ForSeconds1_7 = new WaitForSeconds(1.7f);
    public static readonly WaitForSeconds ForSeconds1_75 = new WaitForSeconds(1.75f);
    public static readonly WaitForSeconds ForSeconds1_8 = new WaitForSeconds(1.8f);
    public static readonly WaitForSeconds ForSeconds1_85 = new WaitForSeconds(1.85f);
    public static readonly WaitForSeconds ForSeconds1_9 = new WaitForSeconds(1.9f);
    public static readonly WaitForSeconds ForSeconds1_95 = new WaitForSeconds(1.95f);

    #endregion
}