using System.Collections;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private AudioPlayer _audioPlayer;
    private AudioSource _audioSource;

    public bool ShouldPlayMusic;

    public float MaxSleep;

    private void Awake()
    {
        ShouldPlayMusic = true;
        _audioPlayer = GetComponent<AudioPlayer>();
        _audioSource = gameObject.AddComponent<AudioSource>();
    }

    private void Start()
    {
        StartCoroutine(PlayMusicCoutine());
    }

    private IEnumerator PlayMusicCoutine()
    {
        while (ShouldPlayMusic)
        {
            _audioSource.clip = _audioPlayer.Clips.SelectRandom();
            _audioSource.Play();

            var duration = Random.Range(5, MaxSleep + 5);

            var t0 = Time.time;

            while (Time.time < t0 + duration)
            {
                yield return Wait.ForEndOfFrame;
            }
        }
    }
}