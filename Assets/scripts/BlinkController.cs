using System.Collections;
using UnityEngine;

public class BlinkController : MonoBehaviour
{
    public float BlinkTime;

    public Color DefaultBlinkColor = new Color(.3f, .3f, .3f);

    private MeshRenderer _meshRenderer;
    private bool _isBlinking;
    private Color _initialColor;

    private void Awake()
    {
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        _initialColor = _meshRenderer.material.color;
    }

    public void Blink(Color color)
    {
        StartCoroutine(BlinkCoroutine(color));
    }

    public void Blink()
    {
        StartCoroutine(BlinkCoroutine(DefaultBlinkColor));
    }

    private IEnumerator BlinkCoroutine(Color color)
    {
        if (_isBlinking)
            yield break;

        _isBlinking = true;

        var t0 = Time.time;
        while (Time.time - t0 < BlinkTime)
        {
            _meshRenderer.material.color += color;
            yield return Wait.ForEndOfFrame;
        }

        t0 = Time.time;
        while (Time.time - t0 < BlinkTime)
        {
            _meshRenderer.material.color -= color;
            yield return Wait.ForEndOfFrame;
        }

        _meshRenderer.material.color = _initialColor;
        _isBlinking = false;
    }
}