using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    public static StatsManager Instance { get; private set; }

    [SerializeField]
    private Stats _stats;

    private void Awake()
    {
        Instance = this;
    }

    public void IncreaseDeath(Role role)
    {
        switch (role)
        {
            case Role.Resistance:
                _stats.ResistanceCasualties++;
                break;
            case Role.Civillian:
                _stats.CivilianCasualties++;
                break;
            case Role.Police:
                _stats.PoliceCasualties++;
                break;
        }
    }

    public void IncreaseDeath(AttackMode mode)
    {
        switch (mode)
        {
            case AttackMode.Melee:
                _stats.MeleeCasualties++;
                break;
            case AttackMode.Shoot:
                _stats.GunCasualties++;
                break;
            case AttackMode.Smoke:
                _stats.SmokeCasualties++;
                break;
            case AttackMode.Molotov:
                _stats.MolotovCasualties++;
                break;
        }
    }

    public void IncreaseSpawns(Role role)
    {
        switch (role)
        {
            case Role.Resistance:
                _stats.ResistanceSpawns++;
                break;
            case Role.Civillian:
                _stats.CivilianSpawns++;
                break;
            case Role.Police:
                _stats.PoliceSpawns++;
                break;
        }
    }

    public void IncreaseBuildingCaptures(Role role)
    {
        switch (role)
        {
            case Role.Resistance:
                _stats.CapturedBuildingResistance++;
                break;
            case Role.Police:
                _stats.CapturedBuildingPolice++;
                break;
        }
    }
}