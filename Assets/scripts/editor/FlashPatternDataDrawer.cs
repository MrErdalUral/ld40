using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PoliceLightFlasher)), CanEditMultipleObjects]
public class FlashPatternDataDrawer : Editor
{
    #region strings
    private const string IndexStr = "Index";
    private const string TimesStr1 = "FlashPatternBlue.Times";
    private const string TimesStr2 = "FlashPatternRed.Times";
    private const string IntervalStr1 = "FlashPatternBlue.Interval";
    private const string IntervalStr2 = "FlashPatternRed.Interval";
    private const string AfterStr1 = "FlashPatternBlue.After";
    private const string AfterStr2 = "FlashPatternRed.After";
    private const string DelayStr1 = "FlashPatternBlue.Delay";
    private const string DelayStr2 = "FlashPatternRed.Delay";
    #endregion

    #region properties
    private SerializedProperty _timesProp1;
    private SerializedProperty _timesProp2;
    private SerializedProperty _intervalProp1;
    private SerializedProperty _intervalProp2;
    private SerializedProperty _afterProp1;
    private SerializedProperty _afterProp2;
    private SerializedProperty _delayProp1;
    private SerializedProperty _delayProp2;
    #endregion

    private void OnEnable()
    {
        SetIfNull(ref _timesProp1, TimesStr1);
        SetIfNull(ref _intervalProp1, IntervalStr1);
        SetIfNull(ref _afterProp1, AfterStr1);
        SetIfNull(ref _delayProp1, DelayStr1);

        SetIfNull(ref _timesProp2, TimesStr2);
        SetIfNull(ref _intervalProp2, IntervalStr2);
        SetIfNull(ref _afterProp2, AfterStr2);
        SetIfNull(ref _delayProp2, DelayStr2);
    }

    private void SetIfNull(ref SerializedProperty property, string str)
    {
        if (property == null)
            property = serializedObject.FindProperty(str);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        DrawFields("Blue Flash Pattern", _timesProp1, _intervalProp1, _afterProp1, _delayProp1);
        DrawFields("Red Flash Pattern", _timesProp2, _intervalProp2, _afterProp2, _delayProp2);

        serializedObject.ApplyModifiedProperties();
    }

    private void DrawFields(string title, SerializedProperty times, SerializedProperty interval, SerializedProperty after, SerializedProperty delay)
    {
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField(title, EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Times");
        times.intValue = EditorGUILayout.IntField(times.intValue);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Interval, After, Delay");
        interval.floatValue = EditorGUILayout.FloatField(interval.floatValue);
        after.floatValue = EditorGUILayout.FloatField(after.floatValue);
        delay.floatValue = EditorGUILayout.FloatField(delay.floatValue);
        EditorGUILayout.EndHorizontal();
    }
}