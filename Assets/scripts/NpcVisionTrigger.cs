﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcVisionTrigger : MonoBehaviour
{

    public NpcShoot NpcShoot;
    public NpcMelee NpcMelee;
    public NpcController NpcController;
    // Use this for initialization
    void Start()
    {
        if (!NpcShoot)
            NpcShoot = GetComponentInParent<NpcShoot>();
        if (!NpcMelee)
            NpcMelee = GetComponentInParent<NpcMelee>();
        if (!NpcController)
            NpcController = GetComponentInParent<NpcController>();
        switch (NpcController.NpcRole)
        {
            case Role.Civillian:
                break;
            case Role.Police:
                gameObject.layer = LayerMask.NameToLayer("PoliceTarget");
                break;
            case Role.Resistance:
                gameObject.layer = LayerMask.NameToLayer("ResistanceTarget");
                break;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        NpcShoot.TargetList.Add(collision.transform);
        NpcMelee.TargetList.Add(collision.transform);
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        NpcShoot.TargetList.Remove(collision.transform);
        NpcMelee.TargetList.Remove(collision.transform);
    }
}
